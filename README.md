# Kerttus bachelor thesis

Repository for all things thesis and more.

## Style guides

Julia ametlik style guide: [link](https://docs.julialang.org/en/v1/manual/style-guide/index.html)

Võib-olla see on lihtsam style guide, mida jälgida: [link](https://github.com/johnmyleswhite/Style.jl)

Neid guide ei pea pähe õppima ega absoluutse tõena võtma, aga kui sa mõnikord kahtled, et kuidas midagi teha või kuidas vormistada, siis nendest võib vastuseid leida, aga alati ei pruugi. Kõige tähtsam on muidugi järjepidevus ja ühtsus.

## Plots

Plots näidised: [link](http://docs.juliaplots.org/latest/generated/gr/)

## Catalogues of 2MRS groups

[Abstract link](https://ui.adsabs.harvard.edu/abs/2015AJ....149..171T/abstract) Tully 2015 "Galaxy Groups: A 2MASS Catalog"

[Abstract link](https://ui.adsabs.harvard.edu/abs/2015AJ....149...54T/abstract) Tully 2015 "Galaxy Groups"

[Abstract link](https://ui.adsabs.harvard.edu/abs/2016ApJ...832...39L/abstract) Lu et al. 2016 "Galaxy Groups in the 2Mass Redshift Survey"

[Abstract link](https://ui.adsabs.harvard.edu/abs/2016yCat..35960014S/abstract) Saulder et al. 2016 "VizieR Online Data Catalog: Group catalogues of the local universe"

[Abstract link](https://ui.adsabs.harvard.edu/abs/2016A%26A...588A..14T/abstract) Tempel et al., 2016 "Friends-of-friends galaxy group finder with membership refinement. Application to the local Universe"

[Abstract link](https://ui.adsabs.harvard.edu/abs/2017MNRAS.470.2982L/abstract) Lim et al. 2017 "Galaxy groups in the low-redshift Universe"

[Abstract link](https://ui.adsabs.harvard.edu/abs/2018A%26A...618A..81T/abstract) Tempel et al., 2018 "Bayesian group finder based on marked point processes. Method and feasibility study using the 2MRS data set"

[Abstract link](https://ui.adsabs.harvard.edu/abs/2020MNRAS.497.2954L/abstract) Lambert et al. 2020 "The 2MASS redshift survey galaxy group catalogue derived from a graph-theory based friends-of-friends algorithm"

## Sisu mustand ja näpunäited

Sissejuhatus

- Selle jaoks ma soovitan sul lugeda lihtsalt paari artikli Introduction peatüki algust.
- Näiteks Lampert et al 2020 ja Tempel et al 2018. Sul ei ole ilmselt vaja nii palju viiteid nagu seal on, aga sellega tegeleme pärast poole. Kõigepealt sisu saad sealt tõlkida ja ümber sõnastada/lihtsustada.

Valdkonna ülevaade

- Kosmoloogilised suurused ehk seletad lahti suuruseid, mida sa kasutad oma töös ja mis võiks olla näiteks sinu kursusekaaslatele liiga keerulised. Näiteks räägid kaugustest, mis skaalal need on ja mis ühikud, samuti massid ja kiirused. Näiteks kuni 1 lk.
- Galaktikad ja grupid ehk räägid hästi lühidalt, mis on galaktikad, nende evolutsioonist ja siis grupid ja nende evolutsioon. Ja siia saad panna ka jutu, et meil ei ole ühest definitsiooni ja saad mõnedest definitsioonidest rääkida. Näiteks 1-2 lk.
- Suureskaalaline struktuur ehk räägid kuidas galaktikad ja grupid on seotud suureskaalalise struktuuriga ja mis nendega seal sees toimub (liiguvad ja liituvad parvedeks ja superparvedeks). Näiteks kuni 1 lk.
- Galaktika gruppide tuvastamine ehk tutvustad üldiselt gruppide vaatlusi ja tuvastamist. Siin saad rääkida mõne lause ajaloost ehk millal tuvastati vaatlustest esimesed grupid ja mis raskusi tekib vaatlustest, näiteks vaatlusefektid nagu Finger of Gods ja kauguse ebatäspsus jne. Näiteks 1 lk.
- 2MRS vaatlusprojekt ehk lühike ülevaade nendest andmetest, mida kõik need kataloogid kasutavad. Näiteks kuni 1 lk.

Gruppeerimise meetodid ja gruppide kataloogid

- Siin saad jaotada meetodid nende lähenemiste järgi alapeatükkideks, anda ülevaade, kuidas need töötavad ja kirjutada ka mõned laused iga kataloogi kohta.

Meetodite võrdlus

- Siin sa räägid teoreetiliselt sellest, kuidas me võrdlema hakkame neid meetodeid omavahel.

Tulemused ja arutelu

- Ja siia tulevad eelmises peatükis välja toodud võrdluste tulemused ning peatüki lõppu ka analüüs ja arutelu, et mida need tulemused tähendavad.

Kokkuvõte

- Lühike kokkuvõte kogu tööst, tood välja ainult tähtsaima. Näiteks 1 lehekülg.

Kõik see võib veel muidugi muutuda, aga annab umbmäärase ülevaate.