# Wrangled data

Group and galaxy catalogues from 6 sources (Tempel fof, Tempel bayes, Lambert, Lim, Lu, Saulder). If a value was not available in the original source the column will be filled with `missing`.

## Galaxy catalogues

Galaxy catalogues. Tempel's catalogues have the most information. Most others are useful for only matching galaxies to groups.

### Columns

1. galID - Local ID for the galaxy, for Lambert's catalogue, this should be that same as 2RMS galaxy IDs
2. groupID - Local ID for the group that galaxy belongs to
3. RA - Right ascension
4. Dec - Declination
5. zobs - Observed redshift, includes proper motion of the galaxy and us
6. zcmb - Redshift corrected to CMB restframe, only include proper of motion of the galaxy
7. dist - Distance from observer in Mpc
8. mag - Magnitude in Ks-band 
9. distgroup - Distance from the group centre

## Group catalogues

### Columns

1. groupID - Local ID for the group
2. RA - Right ascension
3. Dec - Declination
4. zCMB - Redshift in CMB restframe
5. grpdist - Group's centre distance from us in Mpc
6. n_gal - Number of galaxies in the group
7. sigma_v - Standard deviation of velocities in the line of sight in km/s
8. sigma_sky - Standard deviation of position in the ski plane in Mpc
9. r_max - Maximum distance of a galaxy from the group centre in Mpc
10. mass_200 - Mass of the group (inside the volume where the density is 200x the mean density of the Universe)
11. r_200 - Radius of the volume where the density is 200x the mean density of the Universe in kpc
12. mag - Magnitude in Ks-band
