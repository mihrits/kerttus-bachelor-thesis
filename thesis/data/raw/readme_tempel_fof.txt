J/A+A/?/?      Friends-of-friends galaxy group finder.    (Tempel+ 2016)
================================================================================
Friends-of-friends galaxy group finder with membership refinement. Application 
to the local Universe
Tempel, E.; Kipper, R.; Tamm, A.; Gramann, M.; Einasto, M.; Sepp, T.; 
Tuvikene, T.
<Astron. Astrophys. ?, ? (2016)>
================================================================================
ADC_Keywords: Clusters, galaxy ; Fundamental catalog
Keywords: catalogs – galaxies: groups: general – large-scale structure of the 
Universe – methods: data analysis

Abstract:
    We improve the widely used friends-of-friends (FoF) group finding 
    algorithm with membership refinement procedures and apply the method to a 
    combined dataset of galaxies in the local Universe. A major aim of the 
    refinement is to detect subgroups within the FoF groups, enabling a more 
    reliable suppression of the fingers-of-God effect.
    The FoF algorithm is often suspected of leaving subsystems of 
    groups and clusters undetected. We used a galaxy sample built of the 2MRS, 
    CF2, and 2M++ survey data comprising nearly 80 000 galaxies within the 
	local volume of 430 Mpc radius to detect FoF groups. We conducted a 
	multimodality check on the detected groups in search for subgroups. We 
	furthermore refined group membership using the group virial radius and escape 
	velocity to expose unbound galaxies. We used the virial theorem to estimate 
	group masses.
    The analysis results in a catalogue of 6282 galaxy groups in the 
	2MRS sample with two or more members, together with their mass estimates. 
	About half of the initial FoF groups with ten or more members were split into 
	smaller systems with the multimodality check.

File Summary:
--------------------------------------------------------------------------------

  FileName      Lrecl  Records   Explanations
--------------------------------------------------------------------------------

ReadMe           80         .    This file
table1.dat      179     43480    catalog of 2mrs galaxies
table2.dat      179     78378    catalog of combined dataset galaxies
table3.dat      149      6282    catalog of 2mrs groups
table4.dat      149     12106    catalog of combined dataset groups
--------------------------------------------------------------------------------

Byte-by-byte Description of file: table1.dat table2.dat
--------------------------------------------------------------------------------

   Bytes Format  Units   Label     Explanations
--------------------------------------------------------------------------------

  1- 8   I8      ---     pgcid     identification number in PGC
  9- 15  I7      ---     groupid   group id given galaxy belongs to
 16- 20  I5      ---     ngal      richness of the group galaxy belongs to                            
 21- 29  F9.4    Mpc     groupdist comoving distance to the group centre                       
 30- 39  F10.6   ---     zobs      observed redshift                  
 40- 49  F10.6   ---     zcmb      redshift, corrected to the CMB restframe
 50- 59  F10.6   ---     zerr      error of the observed redshift
 60- 68  F9.4    Mpc     dist      comoving distance
 69- 77  F9.4    Mpc     dist_cor  comoving distance, finger-of-god corrected
 78- 87  F10.5   deg     raj2000   right ascension of the galaxy
 88- 97  F10.5   deg     dej2000   declination of the galaxy
 98-107  F10.5   deg     glon      galactic longitude of the galaxy
108-117  F10.5   deg     glat      galactic latitude of the galaxy
118-127  F10.5   deg     sglon     supergalactic longitude
128-137  F10.5   deg     sglat     supergalactic latitude
138-147  F10.4   Mpc     x_sg      supergalactic cartesian coordinate
148-157  F10.4   Mpc     y_sg      supergalactic cartesian coordinate
158-167  F10.4   Mpc     z_sg      supergalactic cartesian coordinate
168-176  F9.4    mag     mag_ks    galactic extinction corrected Ks magnitude
177-179  I3      ---     source    source of the galaxy: 1=2MRS, 2=CF2, 3=2M++
--------------------------------------------------------------------------------

Byte-by-byte Description of file: table3.dat table4.dat
--------------------------------------------------------------------------------

   Bytes Format  Units   Label     Explanations
--------------------------------------------------------------------------------

   1-  7 I7      ---     groupid   group id
   8- 12 I5      ---     ngal      richness of the group
  13- 22 F10.5   deg     raj2000   right ascension of the group centre
  23- 32 F10.5   deg     dej2000   declination of the group centre
  33- 42 F10.5   deg     glon      galactic longitude of the group centre
  43- 52 F10.5   deg     glat      galactic latitude of the group centre
  53- 62 F10.5   deg     sglon     supergalactic longitude  of the group centre
  63- 72 F10.5   deg     sglat     supergalactic latitude  of the group centre
  73- 82 F10.5   ---     zcmb      CMB-corrected redshift of the group
  83- 91 F9.4    Mpc     groupdist comoving distance to the group centre
  92-101 F10.3   km/s    sigma_v   rms deviation of the radial velocities
 102-110 F9.4    Mpc     sigma_sky rms deviation of the sky-projected distances
 111-119 F9.4    Mpc     r_max     distance from group center to farthest member
 120-131 F12.5 10+12Msun mass_200  estimated mass of the NFW profile group
 132-140 F9.4    kpc     r_200     radius, where mean density is 200 the Universe
                                    average density
 141-149 F9.4    mag     mag_group observed magnitude of the group
--------------------------------------------------------------------------------
 
================================================================================
(End)                                                                21-Jan-2016