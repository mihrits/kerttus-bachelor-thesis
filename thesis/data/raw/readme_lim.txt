Here we provide the group catalogs (along with the galaxy catalogs they
are based on) constructed by applying the group finder presented in Lim 
et al. to the four large redshift survey catalogs with the following 
sample selections: 

(we corrected all the redshifts to the CMB rest-frame, assuming the 
heliocenter is moving with a velocity of 368 km/s towards (l,b)=(263.85, 
48.25) with respect to CMB, following Bennett et al. 2003. 
All magnitudes are extinction-corrected based on the dust-map by Schlegel
et al. 1998. We assumed WMAP-9 cosmology throughout the catalogs.
For more details in sample selections and refinements, see the paper. )

(1) 2MASS Redshift Survey (2MRS; Huchra et al. 2012)
   : z <= 0.08
   : Ks <= 11.75 mag

(2) 6dF Galaxy Survey Data Release 3 (6dFGS; Jones et al. 2009) 
   : z <= 0.11
   : Ks,tot <= 12.5 mag  (where Ks,tot is the extrapolated total Ks-band 
                          magnitude as provided in the original 2MRS catalog)

(3) Sloan Digital Sky Survey Data Release 13 (SDSS DR13; Albareti et al. 2016)
   : z <= 0.2
   : r (Petrosian) <= 17.7 mag

(4) 2dF Galaxy Redshift Survey (2dFGRS; Colless et al. 2001)
   : z <= 0.2
   : bJ <= 19.45 mag

   
For galaxies without spectroscopic redshift available from either own survey
or literature, we assign redshifts of their nearest neighbors (z_NN). If 
such a galaxy also has a photometric redshift (z_pho) from the 2MASS Photometric 
Redshift catalog (2MPZ; Bilick et al. 2014) and z_pho differs from z_NN by more
than 12%, the typical error of z_pho, we assign z_pho as the redshif of the 
galaxy instead of z_NN. 

For each of the four surveys, we construct and provide four different sets of 
group (galaxy) catalogs:  

(i) 'survey name'(L): a catalog constructed with galaxies that have 
spectroscopic redshifts, using Proxy-L to estimate halo masses;

(ii) 'survey name'(M): a catalog constructed with galaxies that have 
spectroscopic redshifts, using Proxy-M to estimate halo masses;

(iii) 'survey name'+(L): a catalog constructed with all galaxies, using Proxy-L
to estimate halo masses;

(iv) 'survey name'+(M): and a catalog constructed with all galaxies, using
Proxy-M to estimate halo masses.

For example, there are four different sets of catalogs constructed from the 
2MRS: 2MRS(L), 2MRS(M), 2MRS+(L), and 2MRS+(M), respectively. 


Finally, we use abundance matching to finalize halo masses for groups. 
Note that for flux-limted samples, halos of a given mass are complete only
to a certain redshift. For groups residing in volumes within which the samples 
are not complete (the column "i-o" has a value of 0 for such groups in the 
group catalogs), we used the mean relation between the halo mass and the mass 
proxy from the last iteration to assign halo masses to them. The masses
for such groups should be used with caution depending on scientific goal. 

