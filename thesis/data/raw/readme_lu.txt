
The 2MRS Galaxy Group Catalog
Lu et al., ApJ, in press

catalog version released at 2016 Sep 18.



******************
2MRS :           *
******************

This catalog based on the 2MASS Redshift Survey (Huchra et al, 2012,ApJS, hereafter referred to H12). We have refined the original galaxy catalog for our purpose. The galaxy catalog "2MRS"  contains the total 43533 galaxies in H12, among which only 43246 galaxies within the redshif range: z<=0.08 were linked to our group catalog. You can download the H12 galaxy catalog at "http://tdc-www.harvard.edu/2mrs/" for the more details of these galaxies.
  


I. Selections :
 
  (i)   Ks     <=11.75 mag and detected at H-band
  (ii)  E(B-V) <  1.0  mag
  (iii) |b|    >  5.0  deg for 30 < l < 330 ; |b| > 8.0 deg otherwise

* We corrected all the galaxy redshifs to the Local Group rest frame according to Karachentsev & Makarov(1996). The corrected redshift is represented by 'vlg'. 

* We add the distance information provided by Karachentsev et al. 2013 for some nearby galaxies, including 22 galaxies with negtive redshifts in the galaxy catalog. The distance represented by 'dit'.

* Corrections of Virgo infall are made to 15 galaxies in the front and back of the Virgo cluster according to Karachentsev et al. 2014.

* No K- or E- corrections to galaxy luminosity since the redshift are low.



II. Column :

 1.  ID    : galaxy id 
 2.  NAME  : 2MASS ID in Huchra et al.
 3.  RA    : (J2000.0) in decimal degrees 
 4.  DEC   : (J2000.0) in decimal degrees 
 5.  l     : Galactic longitude
 6.  b     : Galactic latitude
 7.  Ks    : Ks isophotal magnitude, extinction corrected
 8.  H     : same for H
 9.  J     : same for J
 10. v     : redshift
 11. dit   : distance in Mpc (only for nearby gals, others equals to  0.0)
 12. vlg   : redshift corrected to the Local Group rest frame 

* Reading Format (in fortran): 
  format(I5,1x,A16,4(F10.5),3(F7.3),3(F10.1))




******************
2MRS_GROUP :     *
******************

This is Group Catalog based on the '2MRS'. We identifies 29904 groups from a total of of 43,246 2MRS galaxies in the redshift range z <=0.08. Among the groups selected, 5,286 have two or more members, 2,208 are triplets; and 1,189 have four or more members.

I. Selections :

  z <= 0.08
 
II. Columns:

 1. IDG  : group id
 2. Mh   : halo mass in log M_halo/ (M_{\odot}/h)
 3. z    : group redshift (luminosity weighted group center)
 4. idm  : central(brightest) galaxy id.(invalid term)
 5. ID   : central(brightest) galaxy id. (corresponding to 'ID' in '2MRS')

* Reading Format (in fortran):
  format(I5,2(F10.6),2(1x,I5))

* Note that the halo masses are estimated by using the 'GAP' halo mass estimation method (Lu, et al, 2015).




******************
2MRS_MEMBER :    *
******************

This member catalog include all the galaxies linked to the Group Catalog. Only the group id and the corresponding  member galaxy id are given here. please go to the other two catalog for details.

I. Selections :

  z <= 0.08

II. Columns:

 1. IDG : group id (corresponding to 'IDG' in '2MRS_GROUP')
 2. idm : member galaxies id (invalid term) 
 3. ID  : member galaxies id (corresponding to 'ID' in '2MRS')

* Reading Format (in fortran):
  format(I5,1x,I5,1x,I5)

