using DelimitedFiles
using IterTools
 
# vana hea funktsioon vajalike andmete sisse lugemiseks
function get_data_column(file,n) # tahan kätte saada spetsiifilist andmetulpa
   read_file = readdlm(file, comments=true) # loen faili sisse
   column = read_file[:,n] # võtan vajamineva andmetulba
   return column
end
 
function boolean_cat(file2, cat_column) # Schrödingeri kass 
    ra = get_data_column("line_cat_new.txt", 1) # võtab mulle ra tulba
    dec = get_data_column("line_cat_new.txt", 2) # võtab mulle dec tulba
    group_id = get_data_column(file2, 2) # võtab mulle group_ID tulba
    line_gal = get_data_column("line_cat_new.txt", cat_column) # võtab mulle tulba, kus on kirjas, mis real on galaktika mingis valitud kataloogis
    #dest_file = open(dest, "w") # teeb sihtkohtfaili lahti
    # siin toimub kogu paaride tegemine ja otsimine, kas kataloogis on need paarid samas grupis või mitte
    
    ra_dec = [] 
    for (a,b) in zip(ra,dec) #teeb ra ja dec-i väärtustest tuple'id
        rd = (a,b) # vb NamedTuple siia 
        push!(ra_dec,rd)
    end 
    
    bool_dict = Dict()
    i=1 # loendaja

    maxlen = length(ra_dec)

    while i <= maxlen 
        el = ra_dec[i]
        if line_gal[i] === missing
            x = missing
        else 
            print(typeof(line_gal[i]))
            x = convert(Int, line_gal[i])
        end
        j = i + 1
        while j <= maxlen
            if line_gal[j] === missing
                print(line_gal[j])
                y = missing
            else 
                y = convert(Int, line_gal[j])
            end
            j = j + 1
            if y === missing || x === missing # sum((x,y)) === missing
                bool_dict[(el, ra_dec[j-1])] = missing # panin selle juurde, et hiljem oleks analüüs kergem ning iga paari taga oleks mingi väärtus
            elseif group_id[x] == group_id[y] 
                bool_dict[(el, ra_dec[j-1])] = true # kirjutab järjendisse "true" kui galaktikate paar on kataloogis samas grupis
            elseif group_id[x] != group_id[y]
                bool_dict[(el, ra_dec[j-1])] = false # kirjutab järjendisse "false" kui galaktikate paar ei ole kataloogis samas grupis
            end 
        end
        i = i + 1
    end
    return bool_dict
end

function make_cat()
    lamb = boolean_cat("galaxies_lambert_wrangled.txt", 3)
    lim = boolean_cat("galaxies_lim_wrangled.dat", 4)
    lu = boolean_cat("galaxies_lu_wrangled.txt", 5)
    fof = boolean_cat("galaxies_tempel_fof_wrangled.dat", 6)
    bayes = boolean_cat("galaxies_tempel_bayes_wrangled.txt", 7)
    saulder = boolean_cat("galaxies_saulder_wrangled.dat", 8)

    ra1 = []
    dec1 = []
    ra2 = []
    dec2 = []
    for key in keys(lamb)
        push!(ra1, key[1][1])
        push!(dec1, key[1][2])
        push!(ra2, key[2][1])
        push!(dec2, key[2][2])
    end

    open("bool_new_cat.txt", "w") do io # kirjutan galaktikate ra_dec paarid ja vastavad true&false/missing väärtused faili
        writedlm(io, [ra1 dec1 ra2 dec2 values(lamb) values(lim) values(lu) values(fof) values(bayes) values(saulder)])
    end
end
