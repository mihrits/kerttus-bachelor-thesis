# Muutsin ridu 54, 57

using DelimitedFiles
using Statistics

function get_group_data(file,n) # tahan kätte saada spetsiifilist andmetulpa
    read_file = readdlm(file) # loen faili sisse
    group_data = read_file[:,n] # võtan vajamineva andmetulba
    return group_data # jätsin return sisse ikkagi, teeb mulle endale asjad veidi lihtsamaks
end

function get_min_max_mean(data) # Teeb NamedTuple'i min, max ja mean väärtustest
    min_max_mean = (min=minimum(data), max=maximum(data), mean=mean(data))  
    return min_max_mean
end

# Kõik järgmised neli funktsiooni töötavad sarnaselt, funktsioonid on maskidele "ette söötmiseks"
function get_dist_tw(dist) # kontrollib, kas vastav suurus on 0-25% maksimaalsest väärtusest
    if dist <= 0.25*min_max_mean_dist.max
        return true
    else
        return false
    end
end

function get_dist_tw_fi(dist)
    if 0.25*min_max_mean_dist.max < dist <= 0.5*min_max_mean_dist.max
        return true
    else
        return false
    end
end

function get_dist_fi_se(dist)
    if 0.5*min_max_mean_dist.max < dist <= 0.75*min_max_mean_dist.max
        return true
    else
        return false
    end
end

function get_dist_se_hun(dist)
    if 0.75*min_max_mean_dist.max < dist <= min_max_mean_dist.max
        return true
    else
        return false
    end
end

function get_values_for_richness()
    dist = get_group_data("thesis_data_raw_groups_tempel_fof.dat", 10) # loen kauguste andmed sisse
    richness = get_group_data("thesis_data_raw_groups_tempel_fof.dat", 2) # loen gurpis olevate galaktikate arvu andmed sisse
    
    min_max_mean_dist = get_min_max_mean(dist) # leian kauguste miinimumi, maksimumi, keskmise
    min_max_mean_rich = get_min_max_mean(richness) # leian grupis olevate galaktikate arvu miinimumi, maksimumi, keskmise
    
    mask_tw = map(dist -> get_dist_tw, min_max_mean_dist) # teen maski vahemikule 0%-25% kauguste maksimumist
    mask_tw_fi = map(get_dist_tw_fi, dist) # teen maski vahemikule 25%-50% kauguste maksimumist
    mask_fi_se = map(get_dist_fi_se, dist) # teen maski vahemikule 50%-75% kauguste maksimumist
    mask_se_hun = map(get_dist_se_hun, dist) # teen maski vahemikule 75%-100% kauguste maksimumist
    
    min_max_mean_rich_tw = get_min_max_mean(richness[mask_tw,:]) # leian minimaalse, maksimaalse ja keskmise väärtuse vajalikus kauguse vahemikus
    min_max_mean_rich_tw_fi = get_min_max_mean(richness[mask_tw_fi,:]) 
    min_max_mean_rich_fi_se = get_min_max_mean(richness[mask_fi_se,:]) 
    min_max_mean_rich_se_hun = get_min_max_mean(richness[mask_se_hun,:])

    # leian gruppide arvu ka neljas vahemikus siin, et mitte uuesti funktsioone välja kutsuda mingis teises eraldi funktsioonis
    no_groups_tw = count(i-> i==1, mask_tw)
    no_groups_tw_fi = count(i-> i==1, mask_tw_fi)
    no_groups_fi_se = count(i-> i==1, mask_fi_se)
    no_groups_se_hun = count(i-> i==1, mask_se_hun)

    return min_max_mean_rich, min_max_mean_rich_tw, min_max_mean_rich_tw_fi, min_max_mean_rich_fi_se, min_max_mean_rich_se_hun
end

function get_values_for_brightness()

    dist = get_group_data("thesis_data_raw_groups_tempel_fof.dat", 10) # loen kauguste andmed sisse
    brightness = get_group_data("thesis_data_raw_groups_tempel_fof.dat", 16) # loen grupis olevate galaktikate heleduse andmed sisse
    
    global min_max_mean_dist = get_min_max_mean(dist) # leian kauguste miinimumi, maksimumi, keskmise
    min_max_mean_bright = get_min_max_mean(brightness) # leian galaktikate heleduse miinimumi, maksimumi, keskmise
    
    # maskile min_max_mean_dist ette andes hakkab ta selle peal opereerima, kui kirjutada mask_tw = map(dist -> get_dist_tw, min_max_mean_dist), tahaks lihtsalt, et ta teaks, et see on olemas nö
    mask_tw = map(get_dist_tw, dist) # teen maski vahemikule 0%-25% kauguste maksimumist
    mask_tw_fi = map(get_dist_tw_fi, dist) # teen maski vahemikule 25%-50% kauguste maksimumist
    mask_fi_se = map(get_dist_fi_se, dist) # teen maski vahemikule 50%-75% kauguste maksimumist
    mask_se_hun = map(get_dist_se_hun, dist) # teen maski vahemikule 75%-100% kauguste maksimumist
    
    min_max_mean_bright_tw = get_min_max_mean(brightness[mask_tw,:]) # leian minimaalse, maksimaalse ja keskmise väärtuse vajalikus kauguse vahemikus
    min_max_mean_bright_tw_fi = get_min_max_mean(brightness[mask_tw_fi,:]) 
    min_max_mean_bright_fi_se = get_min_max_mean(brightness[mask_fi_se,:]) 
    min_max_mean_bright_se_hun = get_min_max_mean(brightness[mask_se_hun,:]) 

    return min_max_mean_bright, min_max_mean_bright_tw, min_max_mean_bright_tw_fi, min_max_mean_bright_fi_se, min_max_mean_bright_se_hun
end

