using DelimitedFiles

# vana hea funktsioon vajalike andmete sisse lugemiseks
function get_data(file,n) # tahan kätte saada spetsiifilist andmetulpa
    read_file = readdlm(file) # loen faili sisse
    data = read_file[:,n] # võtan vajamineva andmetulba
    return data # jätsin return sisse ikkagi, teeb mulle endale asjad veidi lihtsamaks
end

# funktsioon sorteerib välja grupid, kus on N=>3 galaktikat
function sort(data)
    sorted_groups = []
    for i in data
        if i > 3
            push!(sorted_groups, i)
        end
    end
    return sorted_groups
end

# Kui palju gruppe kokku on? 
function no_of_groups(sorted_groups)
    no_of_groups = length(sorted_groups) # leiab sorteeritud gruppide järjendi pikkuse
end

# Kui paljud galaktikatest kuuluvad gruppi?
function gal_in_group(sorted_groups)
    gal_in_group = sum(sorted_groups) # summeerib järjendi liikmete väärtused
end

# Keskmine, minimaalne, maksimaalne grupi suurus
function get_min_max_mean(sorted_groups) # Teeb NamedTuple'i min, max ja mean väärtustest
    min_max_mean = (min=minimum(sorted_groups), max=maximum(sorted_groups), mean=mean(sorted_groups))  
    return min_max_mean
end

# funktsioon, mis kutsub teisi funktsioone välja, funktsioonide kuningas
function king_function() 
    richness = get_data("thesis_data_raw_groups_tempel_fof.dat", 2) # loen gurpis olevate galaktikate arvu andmed sisse
    sorted_groups = sort(richness)
    groups_no = no_of_groups(sorted_groups)
    group_gal = gal_in_group(sorted_groups)
    min_max_mean = get_min_max_mean(sorted_groups)

    return richness, sorted_groups, groups_no, group_gal, min_max_mean
end
