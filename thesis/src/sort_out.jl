using DelimitedFiles
using DataStructures

# vana hea funktsioon vajalike andmete sisse lugemiseks
function get_data_column(file,n) # tahan kätte saada spetsiifilist andmetulpa
    read_file = readdlm(file, skipstart=1) # loen faili sisse
    column = read_file[:,n] # võtan vajamineva andmetulba
    return column 
end

function get_ID(group_ID_gal) # võtab välja need group_ID väärtused, mis korduvad galaktika failis 3 või enam korda
    id = [] 
    c = counter(group_ID_gal) # loeb kokku, mitu korda mingi group_ID failis kordub, teeb sõnastiku, kus group_ID ja korduvuste arv on üks element
    for (key, value) in zip(keys(c), values(c)) # võtan välja need group_ID-d, mis korduvad 3 või enam korda
        if value >= 3
            push!(id, key)
        end
    end
    return id # tagastab vajalikud group_ID väärtused
end

function filter_gal(ID, group_ID_gal, ra, dec) # filtreerib ra ja dec väärtused group_ID järgi, et oleks need väärtused, mis on kataloogides gruppides meie kriteeriumi järgi
    ra_filt = []
    dec_filt = []
    ra_dec_tuples = []
    for a in ID # võrdleb omavahel juba välja filtreeritud group_ID-sid ning failist võetud group_ID-sid
        i = 1
        for b in group_ID_gal 
            if a == b
                push!(ra_filt, ra[i])
                push!(dec_filt, dec[i])
            end
            i = i+1
        end
    end
    for (a,b) in zip(ra_filt,dec_filt)
        ra_dec = (ra=a,dec=b)
        push!(ra_dec_tuples, ra_dec)
    end
    return ra_dec_tuples # tagastab NamedTuple'i vajalikest galaktikate vajalikest ra ja dec väärtustest
end

function haha(file) # galaktika fail sisse # jooksutab varem defineeritud funktsioone ja tagastab NamedTuple'i ra-dec väärtustest
    group_ID_gal = get_data_column(file, 2)
    ID = get_ID(group_ID_gal)
    ra = get_data_column(file,3)
    dec = get_data_column(file,4)
    ra_dec = filter_gal(ID, group_ID_gal, ra, dec)
    return ra_dec
end

function hehe() # jooksutab järjest haha() funktsiooni ja lõpuks koostab kataloogi ra-dec väärtustest, mis on kõikides kataloogides 3 või enam grupis, lisaks eemaldab duplikaadid
    ra_dec_lamb = haha("galaxies_lambert_wrangled.txt")
    ra_dec_lim = haha("galaxies_lim_wrangled.dat")
    ra_dec_lu = haha("galaxies_lu_wrangled.txt")
    ra_dec_bayes = haha("galaxies_tempel_bayes_wrangled.txt")
    ra_dec_fof = haha("galaxies_tempel_fof_wrangled.dat")
    ra_dec_saulder = haha("galaxies_saulder_wrangled.dat")
    ra_dec = vcat(ra_dec_lamb, ra_dec_bayes, ra_dec_lim, ra_dec_lu, ra_dec_fof, ra_dec_saulder)
    ra_dec_filt = []
    for i in ra_dec
        if !(i in ra_dec_filt)
            push!(ra_dec_filt, i) 
        end
    end
    open("ra_dec_filt.txt", "w") do io
        writedlm(io, ra_dec_filt)
    end
    return ra_dec_filt
end