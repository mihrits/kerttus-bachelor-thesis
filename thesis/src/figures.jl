using DelimitedFiles
using DataStructures
using RollingFunctions
using Distributions
using LinearAlgebra
using Gnuplot


# vana hea funktsioon vajalike andmete sisse lugemiseks
function get_data_column(file,n) # tahan kätte saada spetsiifilist andmetulpa
    read_file = readdlm(file, comments=true) # loen faili sisse
    column = read_file[:,n] # võtan vajamineva andmetulba
    return column
 end

# Gruppide arvtihedus

function cut_cat()

    # bayesi kataloogi galaktikate koordinaadid ja kaugus sisse
    bay_ra = get_data_column("galaxies_tempel_bayes_wrangled.txt", 3) 
    bay_dec = get_data_column("galaxies_tempel_bayes_wrangled.txt", 4)
    bay_dist = get_data_column("galaxies_tempel_bayes_wrangled.txt", 7) # Mpc

    # teen lõike: kääne -30 kuni 60 ja otsetõus 135 kuni 260
    # ehk siis see ülemine "tihedam" koht, sinna jäi rohkem galaktikad, umbes 12 000, sente peale
    cut_dist = []
    for (r, d, dist) in zip(bay_ra, bay_dec, bay_dist) 
        if (135 <= r <= 260) && (-30 <= d <= 60) # kontrollin, kas galaktika ra ja dec mõlemad mahuvad etteantud lõikesse
            push!(cut_dist, dist) # kui mahuvad lõikesse, panen galaktika kauguse listidesse
        end
    end
    return cut_dist
end

function deg2pc(ra,dec,distances)

    # paneb taevakoordinaadid parsekitesse
    pc = []
    for d in distances 
        x = d*cos(deg2rad(dec))*cos(deg2rad(ra)) # kraadid radiaanidesse ja arvutama
        y = d*cos(deg2rad(dec))*sin(deg2rad(ra))
        z = d*sin(deg2rad(dec))
        push!(pc, (x,y,z))
    end  
    return pc
end

function volumes()

    cut_dist_max = ceil(maximum(cut_cat())) # võtab maksimumi lõikes olevate galaktikate kaugusest ja ümardab selle 
    # need kaugused võiksid olla siis iga bini keskelt ehk kui liigume 2 Mpc sammuga, siis 0 ja 2 vahele jääb 1 jne jne  
    distances = range(5, cut_dist_max, step=2) # Mpc

    # arvutan kolm lõike äärepunkti parsekites iga kauguse jaoks
    pc1 = deg2pc(135,-30,distances)
    #pc2 = deg2pc(260,-30,distances) # seda polnud vaja 
    pc3 = deg2pc(135,60,distances)
    pc4 = deg2pc(260,60,distances)

    # leian ristküliku lühema külje pikkuse iga kauguse jaoks
    len_shortedge = []
    for (a,b) in zip(pc1, pc3)
        #3D-s pikkuse leidmise valem 
        d = sqrt((a[1]-b[1])^2+(a[2]-b[2])^2+(a[3]-b[3])^2) 
        push!(len_shortedge, d)
    end

    # leian ristküliku pikema külje pikkuse iga kauguse jaoks
    len_longedge = []
    for (a,b) in zip(pc3, pc4)
        d = sqrt((a[1]-b[1])^2+(a[2]-b[2])^2+(a[3]-b[3])^2) 
        push!(len_longedge, d)
    end

    # leian binide ruumalad
    bin_volumes = []
    for (len_s, len_l) in zip(len_shortedge, len_longedge)
        bin_vol = len_s*len_l*2 # ruumalad kuupmegaparsekites
        push!(bin_volumes, bin_vol)
    end
    return bin_volumes
end

function histo()

    cut_dist = cut_cat()
    # teen kaugused vektoriks hist() funktsiooni jaoks, sest ta tahab seda
    cut_dist = convert(Array{Float64,1}, vec(cut_dist)) 
    bin_volumes = volumes() # kutsun välja binide ruumalasid arvutava funktsiooni
    b_hist = hist(cut_dist, bs=2) # teen histogrammi kasutades lõikes olevaid kaugusi
    
    b_hist_div_v = []
    for (a,b) in zip(b_hist.counts, bin_volumes) 
        c = a/b # jagan binides olevate galaktikate arvu ruumalaga
        push!(b_hist_div_v, c)
    end

    b_hist_div = convert(Array{Float64,1}, vec(b_hist_div_v))
    roll = rolling(sum, b_hist_div, 3, normalize(pdf.(Normal(0,5),[-2,0,2]),1)) # nüüd rullin

    #@gp roll
    
end
