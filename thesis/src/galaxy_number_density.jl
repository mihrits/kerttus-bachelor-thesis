# Moorits Mihkel Muru 2021
# Script to calculate the number density of galaxies as a function of distance

using Pkg

# Check if Julia is running in the correct directory
if !endswith(pwd(), "kerttus-bachelor-thesis/thesis")
    if endswith(pwd(), "kerttus-bachelor-thesis")
        cd("thesis") # if close enough, change to correct directory
    else
        throw(ErrorException("Julia is running in the wrong directory. Change directory to kerttus-bachelor-thesis/thesis."))
    end
end
# Active environment "thesis" to have consistent package versions in Project.toml
Pkg.activate(".")

using DelimitedFiles: readdlm
using StatsBase: fit, Histogram
using RollingFunctions: rolling
using Distributions: pdf, Normal
using LinearAlgebra: normalize

## Read data and make a cut

galdata = readdlm(joinpath("data", "galaxies_tempel_bayes_wrangled.txt"), comments = true)
ra = galdata[:, 3]
dec = galdata[:, 4]
distance = galdata[:, 7] # in Mpc

cut_indeces = @. (135 <= ra <= 260) & (-30 <= dec <= 60)
distance_cut = distance[cut_indeces]

## Calculate volume for each bin

bins = collect(5:2:350)

# http://faraday.uwyo.edu/~admyers/ASTR5160/handouts/51609.pdf
# (ra₂ - ra₁)(sin dec₂ - sin dec₁), (π/180) constant is so I can write ra in degrees
solid_angle = (π/180) * (260-135) * (sin(deg2rad(60)) - sin(deg2rad(-30))) # steradians

# https://en.wikipedia.org/wiki/Solid_angle
# Volume = solid_angle * distance^2 * depth
volumes = @. solid_angle * (bins[1:end-1] + 1)^2 * 2 # Mpc^3

## Calculate histogram and divide each bin by volume

histogram_of_distances = fit(Histogram, distance_cut, bins)
number_density_histogram = histogram_of_distances.weights ./ volumes

## Smooth with a rolling function

number_density_smooth = rolling(sum, number_density_histogram, 3, normalize(pdf.(Normal(0,5),[-2,0,2]),1))
center_of_bins = bins[2:end-2] .+ 1