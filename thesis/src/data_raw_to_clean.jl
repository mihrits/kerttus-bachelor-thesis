### A Pluto.jl notebook ###
# v0.14.7

using Markdown
using InteractiveUtils

# ╔═╡ 4eefe3bc-6cae-11eb-3d9b-2b417638be72
using DelimitedFiles

# ╔═╡ 79dd0cbc-6cae-11eb-183c-7517ca6f2cc9
# For easy access to project directory path
begin
	function projectdir()
		if Base.active_project == Base.load_path_expand("@v#.#")
			@warn "You have not activated current project!"
		end
		dirname(Base.active_project())
	end
	projectdir(args...) = joinpath(projectdir(), args...)
end

# ╔═╡ 6d51620c-6cb0-11eb-36da-49ddd0bda992
DATADIR = projectdir("data", "raw")

# ╔═╡ 2afa1e6e-818d-11eb-237a-612da13eb996
md"""
Gruppide tabelist on vaja alles hoida tulbad:

- groupID
- ra
- dec
- zcmb
- distance to group centre
- ngal
- sigma_v
- sigma_sky
- r_max
- mass_200
- r_200
- mag

Galaktikate tabelist on vaja alles hoida tulbad:

- galID
- groupID
- ra
- dec
- zobs (kui tundub, et zcmb info on paljudel puudu)
- zcmb
- dist
- mag
- groupdist
"""

# ╔═╡ da758d38-818f-11eb-051f-8da2b69f6a02
# Header for group catalogues
header_grp = replace("#groupID RA Dec zCMB grpdist n_gal sigma_v sigma_sky r_max mass_200 r_200 mag", " " => "\t")

# ╔═╡ 82c5bc96-8191-11eb-2af0-3307a2e81273
# Header for galaxy catalogues
header_gal = replace("#galID groupID RA Dec zobs zcmb dist mag distgroup", " " => "\t")

# ╔═╡ 32fce1b2-8194-11eb-0523-1121c547bc74
# Chosen columns for different group catalogues
begin
	groups_tempel_fof_cols = [1, 3, 4, 9, 10, 2, 11, 12, 13, 14, 15, 16]
	groups_tempel_bayes_cols = [1, 3, 4, 9, 10, 2, 11, 12, 13, 14, 15, missing]
	groups_lambert_cols = [1, 3, 4, missing, 11, 7, 13, missing, missing, 16, 15, missing]
	groups_lim_cols = [1, 3, 4, 5, missing, 7, missing, missing, missing, missing, missing, missing]
	groups_lu_cols = [1, missing, missing, 3, missing, missing, missing, missing, missing, missing, missing, missing]
	groups_saulder_cols = [1, 2, 3, 4, missing, 17, 13, missing, 14, missing, missing, 7] # rmax is in kpc, mag is flux and log_10, has "groups" with only one galaxy	
end

# ╔═╡ 75e4befc-8892-11eb-3232-1bd2ed89c2db
# Chosen columns for different galaxy catalogues
begin
	galaxies_tempel_fof_cols = [1, 2, 10, 11, 5, 6, 8, 19, 4]
	galaxies_tempel_bayes_cols = [1, 2, 9, 10, 5, 6, 8, 18, 4]
	galaxies_lambert_cols = [1, 7, 2, 3, missing, missing, missing, missing, missing]
	galaxies_lim_cols = [1, 3, 4, 5, missing, 8, missing, 13, missing] # mag is flux and log_10
	galaxies_lu_cols = [1, 13, 3, 4, 10, missing, 11, 7, missing]
	galaxies_saulder_cols = [1, 2, 4, 5, missing, 6, missing, missing, missing]
end

# ╔═╡ 69e7713c-8b53-11eb-033c-edf94cc57d24
readdlm(joinpath(DATADIR, "galaxies_lu.txt"), comments=true)

# ╔═╡ adbd88bc-8c9e-11eb-3be2-4db6a000b4fe
# Get all the file names starting with eiter "galaxies" or "groups"
begin
	galaxies_files = filter(x -> startswith(x, "galaxies"), readdir(DATADIR))
	groups_files = filter(x -> startswith(x, "groups"), readdir(DATADIR))
	files = append!(galaxies_files, groups_files)
end

# ╔═╡ f13e9ee8-8d41-11eb-16a4-0f774d37152b
begin
	data = Dict(filename => readdlm(joinpath(DATADIR, filename), comments=true) for filename in files)
# Lu's data has membership in separate file, so it needs some special handling...
	membership = readdlm(joinpath(DATADIR, "membership_lu.txt")) # get membership data
	mask = zeros(Int, floor(Int, maximum(membership[:,3]))) # make array of zeros with same length as galaxy array
	for galnum in floor.(Int, membership[:,3]) # if a galaxy ID exists in membership array insert 1
		mask[galnum] = 1
	end
	lu_data = data["galaxies_lu.txt"][Bool.(mask),:] # mask out galaxies that don't belong to groups
	lu_data = hcat(lu_data, floor.(Int, sortslices(membership, dims=1, by=x->x[3])[:,1])) # append group membership to galaxies data
	data["galaxies_lu.txt"] = lu_data # put the data back to dictionary
end; # semicolon for no output

# ╔═╡ f3602998-8d4f-11eb-2b3d-d921fdc31392
# Take a column from data or fill it with missing depending on the column number
function col_data(data, col_num)
	if ismissing(col_num)
		col_data = [missing for _ in 1:size(data,1)]
	else
		col_data = data[:, col_num]
	end
	col_data
end

# ╔═╡ ac3207dc-8d4f-11eb-1258-019e40a4d0c4
# Concatenate the columns together
function wrangle_data(data, cols)
	hcat([col_data(data, col_num) for col_num in cols]...)
end

# ╔═╡ d61efe42-8d4a-11eb-0364-7f00c1269715
function writedata(filename)
	# Choose correct header string
	header = (startswith(filename, "galaxies") ? header_gal : header_grp)
	# Parse new file name (add _wrangled)
	new_filename = filename[1:end-4] * "_wrangled" * filename[end-3:end]
	# Clean, restructure and add missing columns for data
	data_out = wrangle_data(data[filename], @eval $(Symbol(filename[1:end-4] * "_cols")))
	open(projectdir("data", new_filename), "w") do file
		write(file, header * "\n")
		writedlm(file, data_out)
	end
end

# ╔═╡ 18e8ad7e-8d4e-11eb-359f-839534bb0a6a
# Just do it!
for file in files
	writedata(file)
end

# ╔═╡ Cell order:
# ╠═4eefe3bc-6cae-11eb-3d9b-2b417638be72
# ╠═79dd0cbc-6cae-11eb-183c-7517ca6f2cc9
# ╠═6d51620c-6cb0-11eb-36da-49ddd0bda992
# ╟─2afa1e6e-818d-11eb-237a-612da13eb996
# ╠═da758d38-818f-11eb-051f-8da2b69f6a02
# ╠═82c5bc96-8191-11eb-2af0-3307a2e81273
# ╠═32fce1b2-8194-11eb-0523-1121c547bc74
# ╠═75e4befc-8892-11eb-3232-1bd2ed89c2db
# ╠═69e7713c-8b53-11eb-033c-edf94cc57d24
# ╠═adbd88bc-8c9e-11eb-3be2-4db6a000b4fe
# ╠═f13e9ee8-8d41-11eb-16a4-0f774d37152b
# ╠═f3602998-8d4f-11eb-2b3d-d921fdc31392
# ╠═ac3207dc-8d4f-11eb-1258-019e40a4d0c4
# ╠═d61efe42-8d4a-11eb-0364-7f00c1269715
# ╠═18e8ad7e-8d4e-11eb-359f-839534bb0a6a
