using DelimitedFiles
using DataStructures

# vana hea funktsioon vajalike andmete sisse lugemiseks
function get_data_column(file,n) # tahan kätte saada spetsiifilist andmetulpa
   read_file = readdlm(file, skipstart=1) # loen faili sisse
   column = read_file[:,n] # võtan vajamineva andmetulba
   return column
end

function all_ra_dec()
    lamb_ra = get_data_column("galaxies_lambert_wrangled.txt", 3)
    lamb_dec = get_data_column("galaxies_lambert_wrangled.txt",4)
    lim_ra = get_data_column("galaxies_lim_wrangled.dat", 3)
    lim_dec = get_data_column("galaxies_lim_wrangled.dat",4)
    lu_ra = get_data_column("galaxies_lu_wrangled.txt", 3)
    lu_dec = get_data_column("galaxies_lu_wrangled.txt", 4)
    fof_ra = get_data_column("galaxies_tempel_fof_wrangled.dat", 3)
    fof_dec = get_data_column("galaxies_tempel_fof_wrangled.dat", 4)
    bayes_ra = get_data_column("galaxies_tempel_bayes_wrangled.txt", 3)
    bayes_dec = get_data_column("galaxies_tempel_bayes_wrangled.txt", 4)
    saulder_ra = get_data_column("galaxies_saulder_wrangled.dat", 3)
    saulder_dec = get_data_column("galaxies_saulder_wrangled.dat", 4)
    ra = vcat(lamb_ra, lim_ra, lu_ra, fof_ra, bayes_ra, saulder_ra) # lükkan kõik ra ühte järjendisse
    dec = vcat(lamb_dec, lim_dec, lu_dec, fof_dec, bayes_dec, saulder_dec) # lükkan kõik dec ühte järjendisse
    ra_dec = [] # järjend, mis hoiab ra_dec tuple'eid
    for (r, d) in zip(ra,dec) # teen ra_dec tuple'id
        rd = (r, d)
        push!(ra_dec, rd)
    end

    c = counter(ra_dec) # loendan kõik ra_dec tuple'ite esinemised järjendis
    ra_dec_new = [] 
    for (key, value) in c # panen kõik key'd uude järjendisse, see filtreerib välja korduvad väärtused
        push!(ra_dec_new, key) 
    end

    open("ra_dec_new.txt", "w") do io # kirjutan ra_dec tuple'id faili
        writedlm(io, ra_dec_new)
    end
end