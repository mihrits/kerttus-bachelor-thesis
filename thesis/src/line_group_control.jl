using DelimitedFiles
using DataStructures

# vana hea funktsioon vajalike andmete sisse lugemiseks
function get_data_column(file,n) # tahan kätte saada spetsiifilist andmetulpa
   read_file = readdlm(file, comments=true) # loen faili sisse
   column = read_file[:,n] # võtan vajamineva andmetulba
   return column
end

# get_data_column(file, n) = readdlm(file, comments=true)[:, n]

function find_line(cat_file)
    ra = get_data_column("ra_dec_new.txt", 1)
    dec = get_data_column("ra_dec_new.txt", 2)
    ra_cat = round.(get_data_column(cat_file, 3), digits=4)
    dec_cat = round.(get_data_column(cat_file, 4), digits=4)
    group_ID = get_data_column(cat_file, 2)
    rd_cat = []
    c = counter(group_ID)
    for (ra, dec) in zip(ra, dec)
        is_line_found = false
        for (line, (rc, dc, gi)) in enumerate(zip(ra_cat, dec_cat, group_ID))
            if (ra == rc) && (dec == dc) && (c[gi] >= 3)
                push!(rd_cat, line)
                is_line_found = true
            end
        end
        if !is_line_found
            push!(rd_cat, missing)
        end
    end
    return rd_cat
end

function write_file()
    ra = get_data_column("ra_dec_new.txt", 1)
    dec = get_data_column("ra_dec_new.txt", 2)
    lamb = find_line("galaxies_lambert_wrangled.txt")
    lim = find_line("galaxies_lim_wrangled.dat")
    lu = find_line("galaxies_lu_wrangled.txt")
    fof = find_line("galaxies_tempel_fof_wrangled.dat")
    bayes = find_line("galaxies_tempel_bayes_wrangled.txt")
    saulder = find_line("galaxies_saulder_wrangled.dat")

    # kirjutan kataloogi, kus on kirjas, kas galaktika on kuskil kataloogis või mitte
    writedlm("line_cat_new.txt", [ra dec lamb lim lu fof bayes saulder]) 
end

write_file()