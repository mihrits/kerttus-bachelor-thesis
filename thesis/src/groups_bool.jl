using DelimitedFiles
using IterTools
 
# vana hea funktsioon vajalike andmete sisse lugemiseks
function get_data_column(file,n) # tahan kätte saada spetsiifilist andmetulpa
   read_file = readdlm(file, skipstart=1) # loen faili sisse
   column = read_file[:,n] # võtan vajamineva andmetulba
   return column
end
 
function boolean_cat(file2, dest, n, m) # Schrödingeri kass 
   ra = get_data_column("pair_cat.txt", 1) # võtab mulle ra tulba
   dec = get_data_column("pair_cat.txt", 2) # võtab mulle dec tulba
   group_id = get_data_column(file2, n) # võtab mulle group_ID tulba
   line_gal = get_data_column("pair_cat.txt",m) # võtab mulle tulba, kus on kirjas, mis real on galaktika mingis valitud kataloogis
   dest_file = open(dest, "w") # teeb sihtkohtfaili lahti
   # siin toimub kogu paaride tegemine ja otsimine, kas kataloogis on need paarid samas grupis või mitte
   ra_dec = [] 
   for (a,b) in zip(ra,dec)
       rd = (a,b) # vb NamedTuple siia 
       push!(ra_dec,rd)
   end
   
   i=1
   while i <= length(ra_dec)
    el = ra_dec[i]
    if line_gal[i] == "missing"
        x = "missing"
    else 
        x = floor(Int,line_gal[i])
    end
    j = i + 1
    while j <= length(ra_dec)
        if line_gal[j] == "missing"
            y = "missing"
        else 
            y = floor(Int,line_gal[j])
        end
        j = j + 1
        if y == "missing" || x == "missing"
            writedlm(dest_file, [el ra_dec[j-1] "missing"])
        elseif group_id[x] == group_id[y]
            writedlm(dest_file, [el ra_dec[j-1] "true"])
        elseif group_id[x] != group_id[y]
            writedlm(dest_file, [el ra_dec[j-1] "false"])
        end 
    end
    i = i + 1
    end
    close(dest_file)
end
