using DelimitedFiles
using DataStructures

# vana hea funktsioon vajalike andmete sisse lugemiseks
function get_data_column(file,n) # tahan kätte saada spetsiifilist andmetulpa
   read_file = readdlm(file) # loen faili sisse
   column = read_file[:,n] # võtan vajamineva andmetulba
   return column
end

function same_table(bool_cat, c1, c2) #(cat1,cat2)
    bool_1 = get_data_column(bool_cat, c1)
    bool_2 = get_data_column(bool_cat, c2)
    in_same_group=0
    not_in_same_group=0
    mixed = 0
    for (a,b) in zip(bool_1,bool_2)
        if (typeof(a) == SubString{String}) && (typeof(b) == SubString{String})
            continue
        elseif (a==true) && (b==true)
            in_same_group += 1 
        elseif (a == false) && (b==false)
            not_in_same_group += 1
        else
            mixed += 1
        end
    end
    return in_same_group, not_in_same_group, mixed
end
