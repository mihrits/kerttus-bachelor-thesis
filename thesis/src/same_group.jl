using DelimitedFiles
using DataStructures

# vana hea funktsioon vajalike andmete sisse lugemiseks
function get_data_column(file,n) # tahan kätte saada spetsiifilist andmetulpa
   read_file = readdlm(file) # loen faili sisse
   column = read_file[:,n] # võtan vajamineva andmetulba
   return column
end

function gal_same_group()
    ra_dec1 = get_data_column("dest_lamb.txt", 1)[0:100]
    ra_dec2 = get_data_column("dest_lamb.txt", 2)[0:100]
    lu = get_data_column("dest_lu.txt", 3)[0:100]
    lim = get_data_column("dest_lim.txt", 3)[0:100]
    saul = get_data_column("dest_saulder.txt", 3)[0:100]
    fof = get_data_column("dest_fof.txt", 3)[0:100]
    bay = get_data_column("dest_bayes.txt", 3)[0:100]
    lamb = get_data_column("dest_lamb.txt", 3)[0:100]
    file = open("same_group.txt")
    for (l, li, sau, fo, ba, la, rd1, rd2) in zip(lu,lim,saul,fof,bay,lamb,ra_dec1,ra_dec2)
        x = [l,li,sau,fo,ba,la]
        c = counter(x)
        writedlm(file, [rd1 rd2 c["true"]]) 
    end
    close(file)
end

