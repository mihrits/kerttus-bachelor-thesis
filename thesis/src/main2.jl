using DelimitedFiles
using DataStructures

# vana hea funktsioon vajalike andmete sisse lugemiseks
function get_data_column(file,n) # tahan kätte saada spetsiifilist andmetulpa
   read_file = readdlm(file, skipstart=1) # loen faili sisse
   column = read_file[:,n] # võtan vajamineva andmetulba
   return column
end

function pair_cat(ra_cat, dec_cat) # teeb järjendi, kus on kirjas iga galaktika kohta, kas mingis kataloogis on ta kuskil real või teda ei ole 
    ra = get_data_column("ra_dec_filt.txt", 1)
    dec = get_data_column("ra_dec_filt.txt", 2)
    rd_cat = []
    for (r, d) in zip(ra, dec)
        i = 0
        line = 0
        for (rc, dc) in zip(ra_cat, dec_cat)
            line = line+1
            if r == rc && d == dc
                push!(rd_cat, line)
                i = 1
            else 
                continue
            end
        end
        if i == 1
           continue
        else
            push!(rd_cat,"missing")
        end
    end
    return rd_cat
end

function run_pair_cat() # teeb kataloogi, kus on loetletud kõik ra-dec väärtused ja iga kataloogi kohta, mis real galaktika asub või "missing" kui seda pole
    ra = get_data_column("ra_dec_filt.txt", 1)
    dec = get_data_column("ra_dec_filt.txt", 2)
    lim_ra = get_data_column("galaxies_lim_wrangled.dat", 3) 
    lim_dec = get_data_column("galaxies_lim_wrangled.dat", 4)
    lu_ra = get_data_column("galaxies_lu_wrangled.txt", 3)
    lu_dec = get_data_column("galaxies_lu_wrangled.txt", 4) 
    bayes_ra = get_data_column("galaxies_tempel_bayes_wrangled.txt", 3)
    bayes_dec = get_data_column("galaxies_tempel_bayes_wrangled.txt", 4)
    fof_ra = get_data_column("galaxies_tempel_fof_wrangled.dat", 3)
    fof_dec = get_data_column("galaxies_tempel_fof_wrangled.dat", 4)
    saul_ra = get_data_column("galaxies_saulder_wrangled.dat", 3) 
    saul_dec = get_data_column("galaxies_saulder_wrangled.dat", 4)
    lamb_ra = get_data_column("galaxies_lambert_wrangled.txt", 3)
    lamb_dec = get_data_column("galaxies_lambert_wrangled.txt", 4)

    lim = pair_cat(lim_ra, lim_dec)
    lu = pair_cat(lu_ra, lu_dec)
    bayes = pair_cat(bayes_ra, bayes_dec)
    fof = pair_cat(fof_ra, fof_dec)
    saulder = pair_cat(saul_ra, saul_dec)
    lambert = pair_cat(lamb_ra, lamb_dec)

    open("pair_cat.txt", "w") do io
        writedlm(io, [ra dec lim lu bayes fof saulder lambert])
    end

end