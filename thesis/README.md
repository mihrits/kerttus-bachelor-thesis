# Lõputöö README

## TODO

### Kerttu

- [ ] Olemasolevaid min/mean/max funktsioone tagasiside põhjal uuendada
- [ ] Historgrammi, richness-redshift ja gruppide arvu kernel density joonised

### Moorits

- [X] Galaktikate ja gruppide andmeid puhastada ja ühtlustada
- [ ] Overleafis olev läbi lugeda ja tagasisidestada
- [X] Lisada analüüsi ülesandeid
- [ ] Mõelda välja, kas ja kuidas analüüsi juures määramatusi hinnata

## Analüüs harjutamiseks

Ma lisan siia jooksvalt asju, mida me soovime andmetest teada saada / illustreerida.

* Galaktikate arv grupis (~ richness)
  * Minimaalne, keskmine ja maksimaalne väärtus
  * Histogram
  * Minimaalne, keskmine ja maksimaalne väärtus kauguste vahemikus 0-25%, 25-50%, 50-75%, 75-100% max kaugusest
  * Graafik galaktikate arv grupis vs kaugus meist (nt redshift)
* Grupi heledus
  * Samad asjad, mis galaktikate arvu jaoks
* Gruppide arv
  * Gruppide arv neljas erinevas kauguste vahemikus
  * Gruppide arv sõltuvana kaugusest (Kernel density estimation)

Peaaegu kõik need võiks olla kirjutatud funktsioonidena. Funktsioonid võiksid olla üpriski lihtsad ja teha vähe asju korraga. Lõpptulemused saame funktsioonidest, mis koosnevad teistest funktsioonidest.

## Analüüs lõputöös

Iga kataloogi jaoks:

* Kui palju gruppe kokku on?
* Gruppide arvtiheduse sõltuvus kaugusest, kui andmed on olemas
* Kui palju galaktikaid kataloogis on? (See sõltub sellest, mis heleduslõikega kataloogi on kasutatud, kokku on kaks varianti, mitte sellest kui palju galaktikate kataloogist galaktikaid on)
* Kui paljud galaktikatest kuuluvad gruppi?
* Hinnangu gruppi suuruste kohta:
  * Keskmine grupi suurus
  * Minimaalne ja maksimaalne grupi suurus
  * Grupi suuruse sõltuvus kaugusest, kui kauguste andmed olemas

Põhirõhk: Kui võtta kaks erinevat kataloogi, siis kui suur osa galaktikatest kuuluvad samadesse gruppidesse ehk võrdlus kui me valime kaks juhuslikku galaktikat, mis kuuluvad ühes kataloogis samasse gruppi, siis kui suur on tõenäosus, et nad on ka teises kataloogis samas grupis.

Galaktikate cross-matchimiseks kasutame nende ra-dec väärtusi. Kõigepealt on sul vaja koostada kataloog, mis loetleb kõik erinevad ra-dec väärtused ja annab vastavad ridade numbrid erinevate kataloogide jaoks, kust seda galaktikat leida võib või missing kui seda galaktikat seal kataloogis ei ole. Seejärel tuleb kogu see kataloog paarhaaval läbi käia ja kontrollida, et kas see paar asub ühes grupis või mitte ja nii iga gruppide kataloogi ja paari kohta. Ehk tekitada uus kataloog, kus igal real on paar kahest galaktikast ja järgnevates tulpades on true või false olenevalt, kas need on vastavas kataloogis samas grupis või mitte.

## Tagasiside

### 02.03.21

* Julias ei ole tarvidust kirjutada `return`i funktsiooni lõppu, automaatselt väljastatakse viimasel real olev suurus. Aga muidugi ei ole ka vale see return sinna lisada, aitab pärast kergemini pythoni peale tagasi minna.
* Andmete sisselugemine on suhteliselt aeglane tegevus. Suhteline selles mõttes, et kuigi see võtab tõenäoliselt sekundi või vähem, siis see on ~1000 korda aeglasem kui ülejäänud tegevused, mida su funktsioonid teevad. Seega faili võiks ainult ühe korra sisse lugeda ja ülejäänud funktsioonid võiks võtta argumendiks juba sisseloetud andmed, mitte iga kord uuesti andmeid sisse lugeda. Näiteks sul on viimaste funktsioonide juures see viga, et sa loed selle ühe funktsiooni sees kolm korda sama faili sisse.
* Ma soovitaks sul teha funktsiooni, mis võtab argumentideks andmed ja tulba ning tagastab `Tuple` või `NamedTuple`i sellele tulba miinimum, keskmise ja maksimum väärtusega. Seejärel saab selle abil leida kõik sobivad min, mean ja max väärtused.
* Kuna me kasutame neid nelja kauguste vahemikke mitme asja jaoks, siis ma sooviks tekitada vastavad "maskid". See tähendab, et sul on vektor väärtustega `true` (1) ja `false` (0), mis näitab, missugused andmeread jäävad sobivasse kaugustevahemikku. Siis saab sobivad andmed kätte kui kirjutada `data[mask,:]`. Sellisel juhul sa saad maski abil põhimõttelisel uued andmed, mis on lihtsalt vähemate ridadega ja saad uuesti kasutada seda funktsiooni, mis leiab min, mean, max väärtused nende andmete peal.
