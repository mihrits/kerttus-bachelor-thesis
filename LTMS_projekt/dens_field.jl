using DelimitedFiles
using Distances
using Dates
# parameters

x = 1
y = 2
z = 3
mass = 8

function read_file(file) # read the file
    s = readdlm(file, comments = true)
    return s
end

function gen_grid(start, step, stop) # generate the grid
    grid = vec(permutedims(
        [[i, j, k] for i = start:step:stop, j = start:step:stop, k = start:step:stop],[3, 2, 1],))
    return grid
end

function counter(i, tenth_of_fullsize, name) # input as tenth so the function doesn't have to calculate it every time
    i += 1
    if i%tenth_of_fullsize == 0
        println(name * " is $(10*i÷tenth_of_fullsize)% done. Time: "*Dates.format(now(), "HH:MM:SS"))
    end
    i
end

function gal_coord(data) # galaxy coordinates into lists, coordinates into dictionary
    gal_dict = Dict()
    i = 0
    cs = size(data, 1)÷10 # tenth of the full counter size
    for (a, b, c, d) in zip(data[:, x], data[:, y], data[:, z], data[:, mass]) # eachrow et zipi vältida
        i = counter(i, cs, "Galaxy catalogue")
        gal_xyz = [a, b, c]
        gal_dict[gal_xyz] = d # dictionary, key - galaxy coordinates, value - mass
    end
    return gal_dict
end

function find_distances(gal_dict, grid)
    gal_properties = [] # array of dictionaries for every galaxy containing 'properties'
    i = 0
    cs = length(gal_dict)÷10 # tenth of the full counter size
    for (x, mass) in gal_dict
        i = counter(i, cs, "Minimal distance finder")
        gridpoint_array = []
        dist_array = []
        mindist = 99999
        mincrd = (0,0,0)
        for grid_point in grid
            dist = evaluate(Euclidean(), x, grid_point) # parse to make string into int, enumerate didnt work as predicted
            if dist < mindist
                mindist = dist
                mincrd = grid_point
            end
        end
        push!(gal_properties, Dict(
            "coordinate" => x, # coordinates of galaxies
            "gridpoint" => mincrd, # closest gridpoint
            "mass" => mass, # masses
            "distance" => mindist, # distance to the closest gridpoint
            "used" => false, # needed for computing density in grid
        ))
    end
    return gal_properties
end

function grid_with_masses(gal_properties) # adds masses, sums them if needed
    grid_and_masses = []
    i = 0
    cs = length(gal_properties)÷10 # tenth of the full counter size
    for el in gal_properties # compares each galaxy to the entire list of them
        i = counter(i, cs, "Density field")
        if el["used"]
            continue
        end
        total_mass = el["mass"] # if there is only one galaxy in the gridpoint, it's mass is saved
        for op in gal_properties
            if op["gridpoint"] == el["gridpoint"] && op["coordinate"] != el["coordinate"] # makes sure that they arent used, they are at the same gridpoint and are not the same galaxy
                total_mass += op["mass"]
                op["used"] = true
            end
        end
        push!(grid_and_masses, (el["gridpoint"], total_mass)) # makes an array of gridpoints containing mass only
        el["used"] = true
    end
    return grid_and_masses
end

function filled_grid(grid_and_masses, grid) # returns array where elements are arrays containing all gridpoints with respective masses
    i = 0
    cs = size(grid_and_masses,1)÷10 # tenth of the full counter size
    for gal in grid_and_masses # adds masses that are not zero
        i = counter(i, cs, "Grid filler")
            if gal[1] in grid
                index_of_gal = findfirst(isequal(gal[1]), grid)
                push!(grid[index_of_gal], gal[2])
            else
                continue
            end
    end
    i = 0
    cs = size(grid,1)÷10 # tenth of the full counter size
    for grid_point in grid # adds masses that are zero
        i = counter(i, cs, "Grid zero filler")
        if length(grid_point) == 3
            push!(grid_point, 0)
        else
            continue
        end
    end
    return grid
end

function write_file(grid) # writes the output file
    open("densityfield_step05.txt", "w") do io
        write(io, "# x, y, z, density\n")
        writedlm(io, grid)
       end
end

function main()
    data = read_file("/home/moorits/Documents/kerttus-bachelor-thesis/LTMS_projekt/simulation_medium_testset.txt")
    grid = gen_grid(90, 0.5, 150)
    gal_dict = gal_coord(data)
    println("Galaxies are in a dict. Time: "*Dates.format(now(), "HH:MM:SS"))
    gal_properties = find_distances(gal_dict, grid)
    println("Every Galaxy has the closest gridpoint. Time: "*Dates.format(now(), "HH:MM:SS"))
    grid_and_masses = grid_with_masses(gal_properties)
    grid = filled_grid(grid_and_masses, grid)
    println("Grid is done. Time: "*Dates.format(now(), "HH:MM:SS"))
    write_file(grid)
end

#main()
