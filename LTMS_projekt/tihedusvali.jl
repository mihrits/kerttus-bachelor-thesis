using DelimitedFiles
# Kui jooksutada Atom/Juno abil:
# Juno->Working Directory->Current File’s Folder
cd("/home/kerttu/Documents/kerttus-bachelor-thesis/LTMS_projekt")
open("simulation_small_testset.txt", "r") do file
    s=readdlm("simulation_small_testset.txt", header=true)
    rows = countlines("simulation_small_testset.txt", eol = '\n')
    s_transposed = permutedims(s[1], (2,1))
    x = s_transposed[1,:]
    y = s_transposed[2,:]
    z = s_transposed[3,:]
    mass = s_transposed[8,:]
    võrestik = vec(permutedims([(i,j,k) for i in 100:0.1:120, j in 100:0.1:120, k in 100:0.1:120], [3,2,1]))
    gal_coordinates = []
    a = 1
    b = 1
    c = 1
    for (a, b, c) in zip(x, y, z)
        gal_coordinate = tuple(a, b, c)
        a = a + 1
        b = b + 1
        c = c + 1
        push!(gal_coordinates, gal_coordinate)
    end
    galaxies = Dict(gal_coordinates .=> mass)
    distances = []
    for a in gal_coordinates
        for b in võrestik
            distance = sqrt((a[1] - b[1])^2+(a[2]-b[2])^2+(a[3]-b[3])^2)
            push!(distances, distance)
        end
    end
    println(distances)
end
