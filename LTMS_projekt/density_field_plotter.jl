# Moorits Mihkel Muru 2020 UT
# Purpose: cut a slice from density field and plot it
# Usage: julia density_field_plotter.jl density_field.txt galaxies.txt output_file_name
# Density field has to be formatted as 2D array with columns "x y z density"
# Galaxies have to be formatted as 2D array with columns "x y z"

# Stages:
# 1. Read in data from files
# 2. Cut a slice from both data - 5 Mpc width along z axis starting from the center
# 3. Plot density field as heatmap and galaxies as points
# 4. Output plots to .pdf and .png files

####
#### Packages
####

using DelimitedFiles
using Statistics
using Plots
using Dates
using LaTeXStrings


####
#### Globals
####

####
#### Functions
####

function read_data(densityf_file_name, galaxy_catalogue_file_name)
    densityf = readdlm(densityf_file_name, comments = true)
    galaxies = readdlm(galaxy_catalogue_file_name, comments = true)[:, 1:3] # Only x y z
    (densityf, galaxies)
end

function find_slice_range(densityf)
    start = div(densityf[1, 3] + densityf[end, 3], 2) - 2.5 # Median of the 3rd axis
    stop = start + 5
    (start, stop)
end

# Takes array of tuples and turns it into multidimensional array
function tuple_to_mdarray(array)
    permutedims(reduce(hcat, map(collect, array)))
end

function slice(densityf, start, stop)
    densityf[(densityf[:, 3].>start).&(densityf[:, 3].<stop), :]
end

function flatten(densityf, method)
    flattened = Array{Float64,1}[]
    for x in unique(densityf[:, 1])
        for y in unique(densityf[:, 2])
            filtered = densityf[(densityf[:, 1].==x).&(densityf[:, 2].==y), :]
            value = method(filtered[:, 4])
            push!(flattened, [x, y, value])
        end
    end
    tuple_to_mdarray(flattened)
end

function reshape_field(densityf)
    xsize = size(unique(densityf[:, 1]), 1)
    ysize = size(unique(densityf[:, 2]), 1)
    reshape(densityf[:, 3], xsize, ysize)
end

function prep_data(densityf_file_name, galaxy_catalogue_file_name, method)
    densityf, galaxies = read_data(densityf_file_name, galaxy_catalogue_file_name)
    start, stop = find_slice_range(densityf)
    sliced_densityf = slice(densityf, start, stop)
    sliced_galaxy = slice(galaxies, start, stop)
    flattened_densityf = flatten(sliced_densityf, method)
    reshaped_densityf = reshape_field(flattened_densityf)
    (
        reshaped_densityf, # heatmap values in rectangular shape
        unique(flattened_densityf[:, 1]), # x coordinates for heatmap
        unique(flattened_densityf[:, 2]), # y coordinates for heatmap
        sliced_galaxy[:, 1], # x coordinates of galaxy positions
        sliced_galaxy[:, 2], # y coordinates of galaxy positions
    )
end

function plot_data(dfield, dfx, dfy, galx, galy, prefix)
    heatmap(
        dfx,
        dfy,
        log.(dfield.+1),
        ratio = :equal,
        size = (900, 900),
        color = :viridis,
        xlabel = L"x \ (\mathrm{Mpc})",
        ylabel = L"y \ (\mathrm{Mpc})",
        colorbar_title = L"\mathrm{log}(\rho \ (\mathrm{M_{\odot}/Mpc^3}))",
    )
    scatter!(
        galx,
        galy,
        legend = false,
        ms = 3,
        mc = :white,
        msw = 0.5,
        msc = :black,
    )
    filename =
        "fig/" * prefix * "density_field_plot_" * Dates.format(now(), "yyyy-mm-dd_HHMM")
    savefig(filename * ".pdf")
    savefig(filename*".png")
end

####
#### Calculations
####

method = mean
prefix = ""
gogogo = true
if size(ARGS, 1) == 2
    fin_grid, fin_gal = ARGS
elseif size(ARGS, 1) == 3
    fin_grid, fin_gal, prefix = ARGS
elseif size(ARGS, 1) == 4
    gogogo = false
    println("ERROR: Method choosing feature doesn't work.")
    fin_grid, fin_gal, prefix, method = aghs
else
    gogogo = false
end

if gogogo
    dfield, dfx, dfy, galx, galy = prep_data(fin_grid, fin_gal, method)
    plot_data(dfield, dfx, dfy, galx, galy, prefix)
else
    println("ERROR: Wrong number of parameters.")
    println("USAGE: `julia density_field_plotter.jl fin_grid fin_gal prefix method`")
    println("`fin_grid` - input file name for density field grid with columns [x y z density]")
    println("`fin_gal`  - input file name for galaxy positions with columns [x y z ...]")
    println("`prefix`   - (optional) prefix for output file name")
    println(
        "`method`   - (optional) method for flattening density field for plotting" *
        "i.e mean (default), sum, maximum",
    )
end

####
#### Output
####
