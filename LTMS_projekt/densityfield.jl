# Moorits Mihkel Muru 2020
# LTMS projekt "tihedusväli"

# Packages

using DelimitedFiles

# Parameters and constants


# Functions

function readGalData(fileName)
    x = 1
    y = 2
    z = 3
    mass = 8
    readdlm(fileName, comments = true)[:, [x, y, z, mass]] # Loeb andmed sisse ja jätab alles ainult 4 vajalikku tulpa
end

function generateGrid(start, stop, step)
    vec(permutedims([(i, j, k) for i = start:step:stop, j = start:step:stop, k = start:step:stop], [3, 2, 1]))
end

function generateEmptyDensityField(start, stop, step)
    grid = generateGrid(start, stop, step)
    hcat(grid, zeros(size(grid, 1))) # Lisab võrestikule nullidega täidetud veeru
end

function findNearestCoordOnGrid(galaxy, stepsize)
    round.(Int, galaxy[1:3] ./ stepsize) .* stepsize # Ümardab, et leida lähimat võrepunkti
end

function getRowForGalPos(galPosOnGrid, start, stop, step)
    # TODO: Calculate row number based on galaxy position on grid
end

function calculateDensityField(galCoordinates, gridCoordinates, start, stop, step)
    densityField = generateEmptyDensityField(start, stop, step)
    mass = 4
    for galaxy in eachrow(galCoordinates)
        galPosOnGrid = findNearestCoordOnGrid(galaxy, step)
        galRowNumber = getRowForGalPos(galPosOnGrid, start, stop, step)
        densityField[galRowNumber, mass] += galaxy[mass]
    end
    densityField
end

function writeDensityField(fileName, desnistyField)
    open(fileName, 'w') do fout
        write(fout, "# x y z mass") # Pealis väljundfailile
        writedlm(fout, densityField)
    end
end

function main(...)
    # TODO: run everything
end

# Execution

main(...)
