# LTMS projekt 2020 kevad

Projekti raames uurime simulatsiooni tihedusvälja ja gruppeerime SDSS vaatlusprojekti galaktikaid.

## Tihedusvälja kood

All pool on toodud kaks andmefaili, mis on mõeldud koodi testimiseks. Üks neist hästi välja, et kontrollida, kas üldse andmed saavad õigesti sisse loetud ja kood hakkab tööle. Teine sisaldab juba rohkem andmeid ja sellega saab kontrollida, et see asi näeb tihedusvälja moodi välja. Tegelikult on need tõenäoliselt mõlemad piisavalt väikesed, et arvuti loeb need hetkeliselt sisse ja potentsiaalselt ei ole väiksemat isegi vaja.

Koodi osas tuleb sul kõigepealt tekitada võrestik, mille peal tihedusvälja hakkatakse arvutama. Ühe telje peal võiks olla umbes 200 võrepunkti, selle järgi saab arvutada, mis samm olema peaks. Teiseks tuleb välja mõelda meetod, mis võtaks sisse galaktika asukoha ja leiaks talle kõige lähemal oleva võrepunkti. Pärast seda on vaja meetodit, mis lisaks galaktika massi sellesse võrepunkti. Ja lõpuks peab programm väljastama faili, kus on võrepunkti asukoht ja selle kogumass.

Võre tegemiseks ma soovitan kasutada sellist koodi:
```Pyhton
vec(permutedims([(i,j,k) for i in start:step:stop, j in start:step:stop, k in start:step:stop], [3,2,1]))
```
Seal see keskne osa teeb suure kolmemõõtmelise listi, kus on sees Tuple elemendid, kus on info `x`,`y`,`z` väärtuste kohta. `start`, `step` ja `stop` tuleb ise määrata, et algaks ja lõppeks õiges kohas ning oleks sobiva sammuga. Kuna Julias salvestatakse listid tulpade, mitte ridade kaupa, siis tuleb selle suure listi dimensioonide järjekorda vahetada. Seda teebki see `permutedims(list, [3,2,1])` ehk keerab kogu listi tagurpidi. Lõpetuseks, kuna me ei taha 3D listi, vaid 1D listi koordinaatidest, siis kasutame vec käsku, mis teeb sellest tensorist vektori. Niimoodi võiks tulla ilusti õiges järjekorras kõik koordinaadid ette antud alguse, lõpu ja sammuga.

### simulation_small_testset.txt

Piiratud min 100 max 120 Mpc kõigis kolmes teljes
79 andmepunkti

### simulation_medium_testset.txt

Piiratud min 90 max 150 Mpc kõigis kolmes teljes
5590 andmepunkti
