# Moorits Mihkel Muru 2020

using Test

# See tekitab makro, et saaks kirjutada R13"Tere" ja vastuseks saab "Grer"

const lenOfA = 26

macro R13_str(s)
    out = Char[]
    for c in s
        islowercase(c) ? a=collect('a':'z') : a=collect('A':'Z')
        if c ∈ a
            m = mod(findfirst(isequal(c),a)+13,lenOfA)
            push!(out, a[m != 0 ? m : end])
        else
            push!(out, c)
        end
    end
    string(out...)
end


# See osa teeb makrod kõigi erinevate väärtuste jaoks 0st 26ni

for i in 0:26
    macroname = Symbol("R$(i)_str")
    macrobody = :(
                macro $macroname(s)
                    out = Char[]
                    for c in s
                        islowercase(c) ? a=collect('a':'z') : a=collect('A':'Z')
                        if c ∈ a
                            m = mod(findfirst(isequal(c),a)+$(i),lenOfA)
                            push!(out, a[m != 0 ? m : end])
                        else
                            push!(out, c)
                        end
                    end
                    string(out...)
                end
                )
    eval(macrobody)
end

@testset "Tests" begin
    @test R13"abcdefghijklmnopqrstuvwxyz" == "nopqrstuvwxyzabcdefghijklm"
    @test R0"Hello, World!" == "Hello, World!"
    @test R4"Testing 1 2 3 testing" == "Xiwxmrk 1 2 3 xiwxmrk"
    @test R13"abcdefghijklmnopqrstuvwxyz" == "nopqrstuvwxyzabcdefghijklm"
end
