# Moorits Mihkel Muru 2020
# Program to find n-th row of a pascal's triangle and the sum of it

using Test

function pascal(n::Int)::Tuple{Union{Array{Int64,1}, Array{UInt128,1}}, Union{Int64, UInt128}}
       if n <= 0
           println("ERROR: Row number has to be positive integer.")
           return ([0], 0)
       end

       if n > 63 # That mean Int64 is not big enough
           row = [uint128"1"]
       else
           row = [1]
       end

       for i ∈ 1:n-1
           tmp_row = push!(row, 0)
           row = tmp_row .+ reverse(tmp_row)
       end
       (row, sum(row))
end

@testset "Pascal tests" begin
    @test pascal(-1) == ([0], 0)

    @test pascal(1) == ([1], 1)
    @test pascal(5) == ([1, 4, 6, 4, 1], 16)

    @test pascal(100)[2] == int128"2"^(100-1)
end
