function caesar(rot)
  println("Ütle lause, mida soovid (de)šifreerida: ")
  lause = collect(readline(stdin))
  alphabet = Dict{Char,Int64}('a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5, 'f' => 6, 'g' => 7, 'h' => 8, 'i' => 9, 'j' => 10, 'k' => 11, 'l' => 12, 'm' => 13, 'n' => 14, 'o' => 15, 'p' => 16, 'q' => 17, 'r' => 18, 's' => 19, 't' => 20, 'u' => 21, 'v' => 22, 'w' => 23, 'x' => 24, 'y' => 25, 'z' => 26)
  lause_num = []

  for i in lause
    if i in keys(alphabet)
      indeks = alphabet[i]
      uus_indeks = indeks + rot

      if uus_indeks > 26
        uus_indeks = mod(uus_indeks, 26)
      end
      append!(lause_num, uus_indeks)
    else
      append!(lause_num, i)
    end
  end

  output = []

  for i in lause_num
    if i in values(alphabet)
      character = [k for (k, v) in alphabet if v == i][1]
      append!(output, character)
    else
      append!(output, i)
    end
  end
  println(join(output))
end
