function pascal(n)
    println([1]) # et hoida kuvamisviisi ühtsena
    println([1, 1])
    a = [1, 1]
    b = [1, 1]
    i = 1
    while i <= n-2
        i = i + 1
        pushfirst!(a, 0)
        append!(b, 0)
        row = [a[x] + b[x] for x in range(1, stop=length(a))]
        println(row)
        a = copy(row)
        b = copy(row)
    end
end
