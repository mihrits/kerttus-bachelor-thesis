# Programmeerimise harjutused

Valik harjutusi/ülesandeid, et saada programmeerimisega hoog sisse.

## Ülesanded

### Pascal triangle

Kirjuta programm, mis tagastab [Pascali kolmnurga](http://mathworld.wolfram.com/PascalsTriangle.html) `n`-inda rea ja selle summa.

### Sum of sequence

Ülesanne on võetud ülesannete kogust ["Simple Programming Problems"](https://adriann.github.io/programming_problems.html).

Write a program that asks the user for a number `n` and prints the sum of the numbers 1 to `n`.
In addition, modify the previous program such that it also prints sum, where only multiples of three or five are considered in the sum, e.g. 3, 5, 6, 9, 10, 12, 15 for n=17.


### Ceasari cipher

See ülesanne on võetud [exercism](exercism.io) keskkonnast Julia trackilt, ülesande nimi on "Rotational Cipher".

Create an implementation of the rotational cipher, also sometimes called the Caesar cipher.

The Caesar cipher is a simple shift cipher that relies on transposing all the letters in the alphabet using an integer key between 0 and 26. Using a key of 0 or 26 will always yield the same output due to modular arithmetic. The letter is shifted for as many values as the value of the key.

The general notation for rotational ciphers is ROT + <key>. The most commonly used rotational cipher is ROT13.

A ROT13 on the Latin alphabet would be as follows:

```
Plain:  abcdefghijklmnopqrstuvwxyz
Cipher: nopqrstuvwxyzabcdefghijklm
```

It is stronger than the Atbash cipher because it has 27 possible keys, and 25 usable keys.

Ciphertext is written out in the same formatting as the input including spaces and punctuation.

Examples

* ROT5 `omg` gives `trl`
* ROT0 `c` gives `c`
* ROT26 `Cool` gives `Cool`
* ROT13 `The quick brown fox jumps over the lazy dog.` gives `Gur dhvpx oebja sbk whzcf bire gur ynml qbt.`
* ROT13 `Gur dhvpx oebja sbk whzcf bire gur ynml qbt.` gives `The quick brown fox jumps over the lazy dog.`

This is a good exercise to experiment with non-standard string literals and metaprogramming.

A short introduction to non-standard string literals can be found in this blog post. A detailed metaprogramming guide can be found in the manual.

You can extend your solution by adding the functionality described below. To test your solution, you have to remove the comments at the end of runtests.jl before running the tests as usual.

Bonus A only requires basics as outlined in the blog post. Bonus B requires significantly more knowledge of metaprogramming in Julia.

***

Bonus A

Implement a string literal that acts as ROT13 on the string:

```Julia
R13"abcdefghijklmnopqrstuvwxyz" == "nopqrstuvwxyzabcdefghijklm"
```

***

Bonus B

Implement string literals `R<i>, i = 0, ..., 26`, that shift the string for i values:

```Julia
R0"Hello, World!" == "Hello, World!"
R4"Testing 1 2 3 testing" == "Xiwxmrk 1 2 3 xiwxmrk"
R13"abcdefghijklmnopqrstuvwxyz" == "nopqrstuvwxyzabcdefghijklm"
```

***

Vihjeks, et Julias saab teha macrosid kujul näiteks `R13"qwerty"`, kui kasutada sellist definitsiooni

```Julia
macro important_str(s) # Enne alakriipsu on macro nimi ja "_str" ütleb Juliale, et seda saab rakendada stringide peal
	s * "!!!" # Siia läheb macro sisu, ehk kõik mida sa tahad, et see macro ära teeks
end
```

Kui seda macrot kasutada, siis `important"Mine magama" == "Mine magama!!!"`.

